require 'yaml'

$defaults = YAML.load_file('defaults.yaml')
$config = $defaults.merge(YAML.load_file('config.yaml'))


Vagrant.configure("2") do |config|
	config.vm.box = "ubuntu/bionic64"

	config.vm.provider "virtualbox" do |vb|
		vb.gui = true
		vb.memory = $config['memory']
		vb.cpus = $config['cpus']
		vb.customize ['modifyvm', :id, '--vram', '256']
		vb.customize ['modifyvm', :id, '--clipboard', 'bidirectional']
	end

	config.vm.provision 'shell', privileged: false, path: 'provision/base.sh', name: 'base'
	config.vm.provision 'shell', privileged: false, path: 'provision/java.sh', name: 'java'
	config.vm.provision 'shell', privileged: false, path: 'provision/xfce4.sh', name: 'xfce4'
	config.vm.provision 'shell', privileged: false, path: 'provision/odysseus.sh', name: 'odysseus'

	# Optional provisioner: Nexmark
	# call with: --provision-with nexmark
	# e.g.: vagrant up --provision-with shell,nexmark
	config.vm.provision 'nexmark', type: 'shell', privileged: false, path: 'provision/nexmark.sh', name: 'nexmark', run: 'never'

	config.vm.provision 'shell', privileged: false, path: 'provision/local.sh', name: 'local'

	config.vm.provision 'shell', privileged: false, name: 'odysseus-memory', run: 'always', inline: 'if ! [ -z "$1" ]; then sed -i "s/Xmx[[:digit:]]\+M/Xmx${1}M/" ~/odysseus/studio.ini; fi', args: $config['memory_odysseus']
end