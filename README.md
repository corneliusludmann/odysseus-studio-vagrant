# Vagrant-based virtual machine (VirtualBox) for Odysseus Studio

This project provides a [Vagrantfile](https://www.vagrantup.com/docs/vagrantfile/) to run [Odysseus Studio](https://odysseus.uni-oldenburg.de) in a virtual machine.


## Prerequisites

You must have the following installed on your machine:
- [VirtualBox](https://www.virtualbox.org/)
- [Vagrant](https://www.vagrantup.com/)


## Build and start

Run `vagrant up` to build and start the virtual machine. You should restart the virtual machine after provisioning so that all configurations take effect, e.g.:

```
vagrant up && vagrant reload
```


## Build and start with Nexmark

If you want to use the [Nexmark](https://wiki.odysseus.informatik.uni-oldenburg.de/display/ODYSSEUS/Getting+Started+with+Nexmark) data you can activate the nexmark provisioner. This provisioner installs and starts Nexmark as well.

```
vagrant up --provision-with shell,nexmark && vagrant reload
```

Then import nexmark project from `~/nexmark-project/Nexmark` into Odysseus.


## Notes
- Change `config.yaml` and `provision/local.sh` according to your situation.
- Username and password of the system are both 'vagrant'. Normally no login should be required.
- Username of Odysseus user is 'System', password is 'manager' (see [Odysseus Wiki](https://wiki.odysseus.informatik.uni-oldenburg.de/display/ODYSSEUS/How+to+install+Odysseus)).
- Default Odysseus workspace is `/vagrant/workspace`. The content will be stored on the host machine in the `./workspace` folder. You can add your own Odysseus projects or scripts to this folder to access them from inside the virtual machine.
