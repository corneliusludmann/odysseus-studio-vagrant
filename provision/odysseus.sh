#!/bin/sh

# Install dependencies
sudo DEBIAN_FRONTEND=noninteractive apt-get -y install gtk2.0


# Master Build
export ODYSSEUS_URL="http://odysseus.informatik.uni-oldenburg.de/download/products/monolithic/lastBuild/de.uniol.inf.is.odysseus.studio.product.monolithic-linux.gtk.x86_64.zip"
# Development Build
#export ODYSSEUS_URL="http://odysseus.informatik.uni-oldenburg.de/download/products.hourly/origin/development/monolithic/lastBuild/de.uniol.inf.is.odysseus.studio.product.monolithic-linux.gtk.x86_64.zip"

curl -sL $ODYSSEUS_URL --output odysseus.zip
unzip -d ~/odysseus odysseus.zip

mkdir -p ~/.odysseus
tee ~/.odysseus/odysseusRCP.conf <<EOF
<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!DOCTYPE properties SYSTEM "http://java.sun.com/dtd/properties.dtd">
<properties>
<comment>Odysseus Property File edit only if you know what you are doing</comment>
<entry key="__login__user.name">System</entry>
<entry key="__login__user.tenant"/>
<entry key="__login____config__window_show">true</entry>
<entry key="__login__user.password">manager</entry>
<entry key="recentWorkspaces">/vagrant/workspace;</entry>
</properties>
EOF


mkdir -p ~/.local/share/applications/
tee ~/.local/share/applications/odysseus.desktop <<EOF
[Desktop Entry]
Name=Odysseus Studio
Exec=/home/$USER/odysseus/studio
Type=Application
Categories=Development;
EOF
chmod +x ~/.local/share/applications/odysseus.desktop

mkdir -p ~/Desktop
cp ~/.local/share/applications/odysseus.desktop ~/Desktop/
chmod +x ~/Desktop/odysseus.desktop

mkdir -p ~/.config/autostart
cp ~/.local/share/applications/odysseus.desktop ~/.config/autostart/
chmod +x ~/.config/autostart/odysseus.desktop