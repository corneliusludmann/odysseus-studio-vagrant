#!/bin/sh

sudo apt-get update
sudo DEBIAN_FRONTEND=noninteractive apt-get -y install \
	apt-transport-https \
	ca-certificates \
	coreutils \
	curl \
	lsb-release \
	moreutils \
	sed \
	unzip