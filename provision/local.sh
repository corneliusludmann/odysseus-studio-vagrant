#!/bin/sh

# Add/change your local customisation here

sudo timedatectl set-timezone Europe/Berlin

#echo 'XKBLAYOUT="de"' | sudo tee -a /etc/default/keyboard
# even better:
sudo sed -i 's/XKBLAYOUT=\"\w*"/XKBLAYOUT=\"de\"/g' /etc/default/keyboard