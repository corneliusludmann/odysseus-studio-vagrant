#!/bin/sh

curl -sL http://odysseus.informatik.uni-oldenburg.de/download/nexmark/nexmark.linux.gtk.x86_64.zip --output nexmark.zip
unzip nexmark.zip

curl -sL http://odysseus.informatik.uni-oldenburg.de/download/nexmark/NexmarkProject.zip --output nexmark-project.zip
unzip -d ~/nexmark-project nexmark-project.zip
echo "#REQUIRED de.uniol.inf.is.odysseus.parser.cql2.feature.feature.group" | cat - ~/nexmark-project/Nexmark/NexmarkSources.qry | sponge ~/nexmark-project/Nexmark/NexmarkSources.qry
echo "#REQUIRED de.uniol.inf.is.odysseus.parser.cql2.feature.feature.group" | cat - ~/nexmark-project/Nexmark/NexmarkQueries.qry | sponge ~/nexmark-project/Nexmark/NexmarkQueries.qry
tee ~/nexmark-project/Nexmark/NexmarkSourcesPQL.qry <<'EOF'
#PARSER PQL

#QUERY
person = ACCESS({
              source='person',
              wrapper='GenericPush',
              transport='TCPClient',
              protocol='SizeByteBuffer',
              datahandler='Tuple',
              options=[['host', 'localhost'],['port', '65440']],
              schema=[
                ['timestamp', 'STARTTIMESTAMP'],
                ['id', 'INTEGER'],
                ['name', 'STRING'],
                ['email', 'STRING'],
                ['creditcard', 'STRING'],
                ['city', 'STRING'],
                ['state', 'STRING']
              ]
            }
          )

#QUERY
bid = ACCESS({
            source='bid',
            wrapper='GenericPush',
            transport='TCPClient',
            protocol='SizeByteBuffer',
            datahandler='Tuple',
            options=[['host', 'localhost'],['port', '65442']],
            schema=[
              ['timestamp', 'STARTTIMESTAMP'],
              ['auction', 'INTEGER'],
              ['bidder', 'INTEGER'],
              ['datetime', 'LONG'],
              ['price', 'DOUBLE']
            ]
          }
        )

#QUERY
auction = ACCESS({
                source='auction',
                wrapper='GenericPush',
                transport='TCPClient',
                protocol='SizeByteBuffer',
                datahandler='Tuple',
                options=[['host', 'localhost'],['port', '65441']],
                schema=[
                  ['timestamp', 'STARTTIMESTAMP'],
                  ['id', 'INTEGER'],
                  ['itemname', 'STRING'],
                  ['initialbid', 'INTEGER'],
                  ['reserve', 'INTEGER'],
                  ['expires', 'LONG'],
                  ['seller', 'INTEGER'],
                  ['category', 'INTEGER']
                ]
              }
            )

#QUERY
category = ACCESS({
                source='category',
                wrapper='GenericPush',
                transport='TCPClient',
                protocol='SizeByteBuffer',
                datahandler='Tuple',
                options=[['host', 'localhost'],['port', '65443']],
                schema=[
                  ['id', 'INTEGER'],
                  ['name', 'STRING'],
                  ['description', 'STRING'],
                  ['parentid', 'INTEGER']
                ]
              }
            )
EOF


mkdir -p ~/.local/share/applications/
tee ~/.local/share/applications/nexmark.desktop <<EOF
[Desktop Entry]
Name=Nexmark
Exec=/home/$USER/nexmark/nexmark
Type=Application
Categories=Development;
Terminal=true
EOF
chmod +x ~/.local/share/applications/nexmark.desktop

mkdir -p ~/Desktop
cp ~/.local/share/applications/nexmark.desktop ~/Desktop/
chmod +x ~/Desktop/nexmark.desktop

mkdir -p ~/.config/autostart
cp ~/.local/share/applications/nexmark.desktop ~/.config/autostart/
chmod +x ~/.config/autostart/nexmark.desktop