#!/bin/sh

sudo DEBIAN_FRONTEND=noninteractive apt-get -y install xfce4
sudo DEBIAN_FRONTEND=noninteractive apt-get -y install virtualbox-guest-dkms virtualbox-guest-utils virtualbox-guest-x11
echo "$(whoami):$(whoami)" | sudo chpasswd

# Start XFCE on login
tee -a ~/.bash_profile <<'EOF'
if [ -z "$DISPLAY" ] && [ "$(tty)" == /dev/tty1 ]; then
	startx
	sudo VBoxClient --checkhostversion
	sudo VBoxClient --clipboard
	sudo VBoxClient --display
	sudo VBoxClient --draganddrop
	sudo VBoxClient --seamless
fi
EOF

# Autologin user vagrant
sudo mkdir -p /etc/systemd/system/getty@tty1.service.d/
sudo tee /etc/systemd/system/getty@tty1.service.d/override.conf <<EOF
[Service]
ExecStart=
ExecStart=-/sbin/agetty --autologin vagrant --noclear %I $TERM
Type=idle
EOF